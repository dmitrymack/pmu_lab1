package com.example.lab1;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void MaxBiggerA() {
        assertEquals(20, MainActivity.max(20, 4));
    }
    @Test
    public void MaxBiggerB() {
        assertEquals(20, MainActivity.max(4, 20));
    }
    @Test
    public void MinBiggerA() {
        assertEquals(4, MainActivity.min(20, 4));
    }
    @Test
    public void MinBiggerB() {
        assertEquals(4, MainActivity.min(4, 20));
    }
}